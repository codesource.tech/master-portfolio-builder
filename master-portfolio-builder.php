<?php
	
/*
Plugin Name: Master Portfolio Builder
Plugin URI: http://codesource.tech/master-portfolio-builder
Description: The absolute easiest and best plug-in to rapidly build stylish portfolios for your site! Quickly add your portfolio items to your portfolio and publish them to your portfolio. Select your options for style and layout and add it to your page or template using a handy shortcode. 
Version: 1.0
Author: Jeremy Hayes @ codesource.tech
Author URI: http://codesource.tech
License: GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Text Domain: master-portfolio-builder
*/

// =======================TOC ===========================
/* !1. HOOKS 
 * 1.1  register shortcodes on init
 * 1.2 Advanced Custom Fields settings

!2. SHORTCODES 
 * 2.1 register the shortcode
 * 2.2 shortcode layout function

!3. FILTERS 

!4. EXTERNAL SCRIPTS 

!5. ACTIONS

!6. HELPERS 

!7. CUSTOM POST TYPES 

!8. ADMIN PAGES 

!9. SETTINGS */

//========================END TOC============================  

/* !1. HOOKS */

// 1.1  register shortcodes on init
add_action('init', 'mpb_register_shortcodes');

// 1.2 Advanced Custom Fields settings
add_filter('acf/settings/path', 'mpb_acf_settings_path' );
add_filter('acf/settings/dir', 'mpb_acf_settings_dir');
add_filter('acf/settings/show_admin', 'mpb_acf_show_admin');
add_action('init', 'mpb_register_shortcodes');
//if(!defined('ACF_LITE')) define('ACF_LITE', true); //turn off ACF plugin in menu


/* !2. SHORTCODES */

// 2.1 register the shortcode
function mpb_register_shortcodes() {
	add_shortcode('mpb_portfolio', 'mpb_shortcode');
}

// 2.2 shortcode layout function
function mpb_shortcode( $args, $content="") {
		
	// setup output variable - portfolio layout
	$output = 
		'<div class="mpb">
			<section><h3>Portfolio</h3> </section>
			<div class="mpb-carousel-wrap">
				<div class="mpb-carousel">';

// start portfolio image loop	
$loop = new WP_Query( array( 'post_type' => 'portfolio', 'orderby' => 'post_id', 'order' => 'ASC' ) );
while( $loop->have_posts() ) : $loop->the_post();

// get an image field
$portfolio_image = get_field('portfolio_image');
				
// each image contains a custom field called 'link'
$link = get_field('link');
//USE heredoc to add single quotes and # to the string to get it ready to pass it to js
$link2 = <<<EOT
'$link'
EOT;
// render
				
	$output .= '	<div>
						<button class="portfolioButton" onclick="togglePortfolio(' . $link2 . ')"><img src="' . $portfolio_image['url'] . '"></button>
					</div>
					';
	
endwhile; //end portfolio image loop

	$output .= '							
				</div>
			</div>
		</div>';
		
// start portfolio hidden divs loop
$loop = new WP_Query( array( 'post_type' => 'portfolio', 'orderby' => 'post_id', 'order' => 'ASC' ) );
while( $loop->have_posts() ) : $loop->the_post();

// each image contains a custom field called 'link'
$portfolio_image2 = get_field('portfolio_image2');
				
$portfolio_video = get_field('portfolio_video');
				
$link = get_field('link');
//USE heredoc to add single quotes and # to the string to get it ready to pass it to js
$link2 = <<<EOT
'$link'
EOT;

$portfolio_page = get_field('portfolio_page');
				
$portfolio_title = get_field('portfolio_title');

		$output .= 
		'<div class="mpb_portfolio_item">
		<div id="' . $link . '" style="display:none;">
		<button class="btn btn-default closePort" onclick="togglePortfolio(' . $link2 . ')"><i class="fa fa-close"></i></button> 
		<h3 style="text-align: center;">' . $portfolio_title . '</h3> <div class="mpb-port-img">
		';
		if( !empty($portfolio_image2) ) :
			$output .='<img src="' . $portfolio_image2['url'] .'" />';
		endif;
		if( !empty($portfolio_video) ) :
			$output .= $portfolio_video['url'];
		endif;
		$output .= '</div>
			<p style="text-align: center;">'. $portfolio_page . '</p>
			
		</div>
		</div>';
endwhile; //end portfolio hidden div loop
	// return the output code
	return $output;
	
}

/* !3. FILTERS */

/* !4. EXTERNAL SCRIPTS */


	// register scripts with wordpress
	wp_register_style('master-portfolio-builder-css-public', plugins_url('/css/public/master-portfolio-builder.css',__FILE__));
	
	//add scripts to page load que
	wp_enqueue_style('master-portfolio-builder-css-public');
	
	
	//add bootstrap css to wordpress que
	function mpb_load_external()
	{
	    // Register the script for this theme:
	    wp_register_style('mpb-slick-css', plugins_url('/slick/slick.css',__FILE__));
		wp_register_style('mpb-slick-theme-css', plugins_url('/slick/slick-theme.css',__FILE__));
		wp_register_style('mpb-fontawesome-css', plugins_url('/css/public/font-awesome.css',__FILE__));
		wp_register_script('jquery', plugins_url('/scripts/jquery.min.js',__FILE__));
		wp_register_script('mpb-slick-js', plugins_url('/slick/slick.min.js', __FILE__), array( 'jquery' ));
		wp_register_script('master-portfolio-builder-js', plugins_url('/scripts/portfolio.js', __FILE__), array( 'jquery' ));
	    
		//add portfoilo.js to wordpress que
		wp_enqueue_script( 'jquery');
		wp_enqueue_style('mpb-slick-css');
		wp_enqueue_style('mpb-slick-theme-css');
		wp_enqueue_style('mpb-fontawesome-css');
	    wp_enqueue_script('mpb-slick-js');
		wp_enqueue_script('master-portfolio-builder-js');
	}
	

	
/* !5. ACTIONS */

add_action( 'wp_enqueue_scripts', 'mpb_load_external' );

/* !6. HELPERS */


/* !7. CUSTOM POST TYPES */

include_once(plugin_dir_path(__FILE__) . 'lib/advanced-custom-fields/acf.php' );

include_once( plugin_dir_path(__FILE__) . 'cpt/mpb_portfolio.php');



/* !8. ADMIN PAGES */

/* !9. SETTINGS */



